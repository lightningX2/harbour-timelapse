# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-timelapse

CONFIG += sailfishapp

SOURCES += src/harbour-timelapse.cpp \
    src/app/core/init.cpp \
#    src/app/core/localdb.cpp \
    src/app/core/uicontrol.cpp \
    src/sys/core/paramhandler.cpp \
    src/sys/core/syslogger.cpp \
#    src/sys/core/dbmodule.cpp \
    src/app/modules/Timelapse/timelapse.cpp \
    src/app/modules/Timelapse/encodeprocessor.cpp

OTHER_FILES += qml/harbour-timelapse.qml \
    qml/cover/CoverPage.qml \
    qml/pages/SecondPage.qml \
    rpm/harbour-timelapse.changes.in \
    rpm/harbour-timelapse.spec \
    rpm/harbour-timelapse.yaml \
    translations/*.ts \
    harbour-timelapse.desktop  \
    qml/img/play-button.png \
    qml/img/stop-button.png


SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

ffmpeg.files = harbour-timelapse-ffmpeg.xz
ffmpeg.path = /usr/share/harbour-timelapse/

INSTALLS += ffmpeg

CONFIG +=c++11
QT += xml sql

INCLUDEPATH += "src/"

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-timelapse-de.ts \
                translations/harbour-timelapse-ru.ts

HEADERS += \
    src/app/core/init.h \
#    src/app/core/localdb.h \
    src/app/core/structs.h \
    src/app/core/uicontrol.h \
    src/sys/core/paramhandler.h \
    src/sys/core/syslogger.h \
#    src/sys/core/dbmodule.h \
    src/app/modules/Timelapse/timelapse.h \
    src/app/modules/Timelapse/encodeprocessor.h

DISTFILES += \
    qml/pages/ShootScreen.qml \
    qml/utils/localdb.js

#RESOURCES += \
#    res.qrc
