#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <QGuiApplication>
#include <sailfishapp.h>
#include "app/core/init.h"

int main(int argc, char *argv[])
{

    //test
    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    QTranslator translator;
    if (translator.load("harbour-timelapse-ru",// + QLocale::system().name(),
                    "/usr/share/harbour-timelapse/translations"))
        qDebug("LOADED RU");
    app->installTranslator(&translator);
    qDebug("LOCALE: "+QLocale::system().name().toUtf8());
    Init w;
    qDebug("init w");
    w.prepareApp();
    qDebug("prepareApp");
    return app->exec();
}

