#include "localdb.h"

LocalDB::LocalDB(QObject *parent) :
    QObject(parent)
{

}

bool LocalDB::init(){
    checkTables();

//    if (!db.open())
//        SysLogger::addError("db can't open");
    return false;
}

void LocalDB::checkTables(){
    dbm->execSQL("CREATE TABLE songs(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                 "AUTHOR TEXT, NAME TEXT, LYRICS TEXT, ALBUM INTEGER, FILENAME TEXT,"
                 "INVK INTEGER, CACHED INTEGER, BITRATE INTEGER, SIZE INTEGER)");
    dbm->execSQL("CREATE TABLE albums(ID INTEGER PRIMARY KEY,"
                 "NAME TEXT)");
    dbm->execSQL("CREATE TABLE config(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                 "code TEXT, value TEXT");
}



