#ifndef LOCALDB_H
#define LOCALDB_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QStandardPaths>
#include <QHash>

#include "sys/core/dbmodule.h"
#include "sys/core/syslogger.h"
#include "sys/core/paramhandler.h"


class LocalDB;

class LocalDB : public QObject
{
    Q_OBJECT
public:
    dbmodule *dbm;
    void setDb(dbmodule * db);
    explicit LocalDB(QObject *parent = 0);
    bool init();

private:



    void checkTables();

signals:

public slots:

};

#endif // LOCALDB_H
