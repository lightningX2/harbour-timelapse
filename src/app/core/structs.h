#ifndef STRUCTS_H
#define STRUCTS_H
#include <QString>

class SongsInfo;

class SongsInfo {

public:
    QString id;
    QString artist;
    QString name;
    QString lyrics;
    bool inVK;
    bool cached;
    qint64 bitrate;
    qint64 size;
};



class AlbumInfo {
public:
    QString idAlbum;
    QString title;
    QString owner;
};



#endif // STRUCTS_H
