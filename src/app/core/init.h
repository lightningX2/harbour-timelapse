#ifndef INIT_H
#define INIT_H

#include <QObject>
#include <QQmlContext>
#include <QQmlEngine>
#include <QtQml>
#include <QQuickView>
#include <sailfishapp.h>

#include "uicontrol.h"
#include "app/modules/Timelapse/timelapse.h"

//#include "app/modules/vk/vk.h"
//#include "localdb.h"
//#include "app/modules/auth/auth.h"
//#include "sys/core/dbmodule.h"

class Init : public QObject
{
    Q_OBJECT
public:
    explicit Init(QObject *parent = 0);
    void prepareApp();
signals:

private:
    QQuickView *pView;

    UIControl *pUIController;
    Timelapse *pTimelapse;
    const QString MODULE_NAME = "INIT";

//    LocalDB db;
//    dbmodule dbm;

public slots:
    void termination();
};

#endif // INIT_H
