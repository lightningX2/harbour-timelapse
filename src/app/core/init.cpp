#include "init.h"
#include "sys/core/paramhandler.h"

Init::Init(QObject *parent) :
    QObject(parent)
{
    SysLogger::init();
    SysLogger::setLevel(2);
    SysLogger::setDebugPanelShowed(false);

    pUIController = new UIControl(this);
    pTimelapse = new Timelapse(this);
}

void Init::prepareApp(){
    ParamHandler::init();
    ParamHandler::setSysDat("app.type","timelapse");
    ParamHandler::setSysDat("app.mode","stable");
    ParamHandler::setSysDat("app.version","0.5.3");



    pView = SailfishApp::createView();

    pView->rootContext()->setContextProperty("SysLogger",SysLogger_impl::instance());
    pView->rootContext()->setContextProperty("UILink",pUIController);
    pView->rootContext()->setContextProperty("TimelapseLink",pTimelapse);

    pView->setSource(SailfishApp::pathTo("qml/harbour-timelapse.qml"));

    pView->show();
    SysLogger::addInfo("offline storage "+pView->engine()->offlineStoragePath(),2);
//    qApp->processEvents();
    SysLogger::addInfo("before core",2);
    pUIController->init();
    SysLogger::addInfo("initialized core",2);
    connect(pUIController,&UIControl::requestedTerminate,this,&Init::termination);


}

void Init::termination(){
//    SysLogger::addInfo("requested exit",2);
    pView->close();
    qApp->quit();
}
