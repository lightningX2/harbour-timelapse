#ifndef UICONTROL_H
#define UICONTROL_H

#include <QObject>
#include <QKeyEvent>

#include "sys/core/syslogger.h"

class UIControl : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool sdAvailable READ getSdAvailable NOTIFY sdAvailableChanged)

private:

    int debugActivated;
    bool m_sdAvailable;

    void setSDAvailable(bool newVal);

public:
    UIControl(QObject *parent = 0);


    void init();

    Q_INVOKABLE void keyHandler(int key, int modifiers);


    bool getSdAvailable() const{
        return m_sdAvailable;
    }

signals:
    void requestedTerminate();
    void requestedReplacePage(QString page);

    void sdAvailableChanged();

public slots:

    QString getSDPath();

};

#endif // UICONTROL_H
