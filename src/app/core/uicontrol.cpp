#include "uicontrol.h"
#include <QStringList>

void UIControl::setSDAvailable(bool newVal){
    m_sdAvailable = newVal;
    emit sdAvailableChanged();
}

UIControl::UIControl(QObject *parent){

    debugActivated=0;
}

void UIControl::init(){
    QString sdcardpath = getSDPath();
    SysLogger::addInfo("sdpath:"+sdcardpath,2);
    if (sdcardpath.isEmpty()){
        setSDAvailable(false);
        return;
    }
    setSDAvailable(true);
    QDir(sdcardpath).mkdir("Timelapse");

}

void UIControl::keyHandler(int key,int modifiers){
    SysLogger::addInfo("pressed "+QString::number(key),2);
}

QString UIControl::getSDPath(){
    QStringList posibleSdCardMountsList = QDir("/media/sdcard/").entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    if (posibleSdCardMountsList.count()==0)
        return "";
    return QString("/media/sdcard/"+posibleSdCardMountsList.first());
}
