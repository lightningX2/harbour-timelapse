#ifndef TIMELAPSE_H
#define TIMELAPSE_H

#include <QObject>
#include <QTimer>
#include <QDateTime>
#include <QProcess>
#include <QThread>
#include "encodeprocessor.h"
#include "sys/core/syslogger.h"

class Timelapse;

class Timelapse : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool busyEncoding READ getBusyEncoding NOTIFY busyEncodingChanged)
public:
    explicit Timelapse(QObject *parent = 0);
private:

    QTimer recordTimer;
    int timeout;
    int recID;
    QString savePath;
    QString subfolder;
    QThread *m_encodeThread;
    EncodeProcessor *encodeProcessor;

    bool busyEncoding;

private slots:
    void doRec();
    void onFinishedEncoding();
    bool getBusyEncoding(){return busyEncoding;}
    void setBusyEncoding(bool newVal);

signals:
    void requestedShooting(QString picturePath);
    void requestRunNotify(QString moviePath);
    void doEncode(QString path, QString subpath);
    void busyEncodingChanged();

public slots:
    void setSavePath(QString path);
    void setTimeout(int sec);
    void startRecord();
    void stopRecord();

};

#endif // TIMELAPSE_H
