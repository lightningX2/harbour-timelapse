#include "encodeprocessor.h"
#include <QStandardPaths>

EncodeProcessor::EncodeProcessor(QObject *parent) : QObject(parent)
{

}


//convert from abc text to Morse code
void EncodeProcessor::doEncode(QString savePath, QString subfolder){
    QMutexLocker locker(&m_mutex);
    //check exist decoder, decompress to data
    QString ffmpegPath = QStandardPaths::writableLocation(QStandardPaths::DataLocation)+"/ffmpeg_armv7";
    if (!QFile(ffmpegPath).exists()){

        QProcess::execute("sh",QStringList()<<"-c" <<"xz -dc "
                          "/usr/share/harbour-timelapse/harbour-timelapse-ffmpeg.xz "
                          "> "+ffmpegPath);
        QProcess::execute("chmod",QStringList()<<"744"
                          <<ffmpegPath);
    }
    SysLogger::addInfo("emitted encodeProcessor",2);
//   -f image2 -pattern_type glob -i '2016-08-04-21-12/*.jpg' -c:v libx264
    //-vf scale=1920:-1 -refs 7 -threads 3 -pix_fmt yuv420p
    if (QProcess::execute(ffmpegPath,QStringList()<<"-f"<<"image2"
                          <<"-pattern_type"<< "glob" <<"-i" <<
                          ""+savePath+subfolder+"/*.jpg"
                          <<"-c:v"<< "libx264" <<"-vf"
                          <<"scale=1920:-1"<< "-refs"<<"7"
                          <<"-threads"<<"2"<<"-pix_fmt"<< "yuv420p"
                          <<savePath+subfolder+".mp4")==0){
        SysLogger::addInfo("deleting photos",2);
        QDir dir(savePath+subfolder);
        dir.removeRecursively();
    } else {
        SysLogger::addError("encode process failed");
    }


    emit onFinished();
}
