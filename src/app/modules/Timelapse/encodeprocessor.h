#ifndef ENCODEPROCESSOR_H
#define ENCODEPROCESSOR_H

#include <QObject>
#include <QHash>
#include <QMutex>
#include <QProcess>
#include "sys/core/syslogger.h"

class EncodeProcessor : public QObject
{
    Q_OBJECT
public:
    explicit EncodeProcessor(QObject *parent = 0);
private:

    QMutex m_mutex;

signals:
    void onFinished();

public slots:
    void doEncode(QString savePath, QString subfolder);

};

#endif // ENCODEPROCESSOR_H
