#include "timelapse.h"

Timelapse::Timelapse(QObject *parent) : QObject(parent)
{
    connect(&recordTimer,&QTimer::timeout,this,&Timelapse::doRec);
    timeout=60;
    encodeProcessor = new EncodeProcessor();
    m_encodeThread = new QThread(this);
    encodeProcessor->moveToThread(m_encodeThread);

    connect(this,&Timelapse::doEncode,encodeProcessor,&EncodeProcessor::doEncode);
    connect(encodeProcessor,&EncodeProcessor::onFinished,this,&Timelapse::onFinishedEncoding);

    m_encodeThread->start(QThread::LowestPriority);
    setBusyEncoding(false);
}

void Timelapse::doRec(){
    QString picturePath = savePath+subfolder+"/"+QString::number(QDateTime::currentMSecsSinceEpoch())+".jpg";
    SysLogger::addInfo("shooting "+picturePath);
    requestedShooting(picturePath);
}

void Timelapse::onFinishedEncoding(){
    SysLogger::addInfo("finish slot emitted");
    setBusyEncoding(false);
    emit requestRunNotify(subfolder+".mp4");
}

void Timelapse::setBusyEncoding(bool newVal){
    busyEncoding = newVal;
    emit busyEncodingChanged();
}

void Timelapse::setSavePath(QString path){
    savePath = path;
}

void Timelapse::setTimeout(int sec){
    timeout = sec;
}

void Timelapse::startRecord(){
    recID=0;
    subfolder = QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm");
    QDir().mkpath(savePath+subfolder);
    //    QDir(savePath).mkdir(subfolder);
    SysLogger::addInfo("started record with timeout "+QString::number(timeout));
    recordTimer.start(timeout*1000);
}

void Timelapse::stopRecord(){
    recordTimer.stop();
    SysLogger::addInfo("stopped");
    setBusyEncoding(true);
    emit doEncode(savePath,subfolder);
//    QProcess test;


//    if (QProcess::execute("ffmpeg_static",QStringList()<<"-f"<<"image2"
//                          <<"-pattern_type"<< "glob" <<"-i" <<
//                          ""+savePath+subfolder+"/*.jpg"
//                          <<"-c:v"<< "libx264" <<"-pix_fmt"<< "yuv420p"
//                          <<savePath+subfolder+".mp4")==0){
//        SysLogger::addInfo("deleting photos",2);
//        QDir dir(savePath+subfolder);
//        dir.removeRecursively();
//    } else {
//        SysLogger::addError("encode process failed");
//    }
}
